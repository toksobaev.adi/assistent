import { createApi } from "@reduxjs/toolkit/query/react";
import { baseQuery } from "../../utils/baseQuery";
import { injectToken } from "../../utils/injectToken";
import { CounterResult, GuidBaseResult, PatientDto, PatientDtoArrayBaseResult, RecipeDto, RecipeDtoArrayBaseResult } from "models/index";

export const patientApi = createApi({
  reducerPath: "patientApi",
  baseQuery,
  tagTypes: ["Patients", "Recipes"],
  refetchOnFocus: true,
  endpoints: (build) => ({
    patients: build.query<PatientDtoArrayBaseResult, string | undefined>({
      query: (searchText?: string) => ({
        url: "/Patient/GetPatients",
        headers: injectToken(),
        params: {
          searchText,
        },
      }),
      providesTags: (data) =>
      data
          ? [...data.result.map(({ id }) => ({ type: 'Patients' as const, id })), 'Patients']
          : ['Patients'],
    }),
    setPatient: build.mutation<GuidBaseResult, PatientDto>({
      query: (patient) => ({
        url: `/Patient/SetPatient`,
        headers: injectToken(),
        method: "POST",
        body: {...patient },
      }),
      invalidatesTags: ['Patients'],
    }),
    counters: build.query<CounterResult, string>({
      query: () => ({
        url: "/Patient/GetCounters",
        headers: injectToken(),
      }),
      providesTags: ['Patients'],
    }),
    recipes: build.query<RecipeDtoArrayBaseResult, string | undefined>({
      query: (patientId) => ({
        url: "/Patient/GetRecipes",
        headers: injectToken(),
        params: { patientId }
      }),
      providesTags: (data) =>
      data
          ? [...data.result.map(({ id }) => ({ type: 'Recipes' as const, id })), 'Recipes']
          : ['Recipes'],
    }),
    setRecipe: build.mutation<GuidBaseResult, RecipeDto>({
      query: (recipe) => ({
        url: `/Patient/SetRecipe`,
        headers: injectToken(),
        method: "POST",
        body: {...recipe },
      }),
      invalidatesTags: ['Recipes'],
    })
  }),
});

export const { usePatientsQuery, useSetPatientMutation, useCountersQuery, useRecipesQuery, useSetRecipeMutation } = patientApi;
