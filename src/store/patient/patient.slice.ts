import { PayloadAction, createSlice } from "@reduxjs/toolkit";
import { Medicament, Patient, RecipeDto, RecipeMedicament } from "models/index";
import { removeItemAtIndex, replaceItemAtIndex } from "utils/replaceItem";

interface PatientState {
  currentPatient: Patient | null;
  newRecipeMedicaments: RecipeMedicament[];
  currentRecipeMedicament: RecipeMedicament | null;
  currentRecipe: RecipeDto | null;
  medicament: Medicament | null;
}

const initialState: PatientState = {
  currentPatient: JSON.parse(`${localStorage.getItem("currentPatient")}`),
  newRecipeMedicaments:
    JSON.parse(`${localStorage.getItem("newRecipeMedicaments")}`) || [],
  currentRecipeMedicament: null,
  currentRecipe: JSON.parse(`${localStorage.getItem("currentRecipe")}`),
  medicament: null,
};

export const patientSlice = createSlice({
  name: "patient",
  initialState,
  reducers: {
    addPatient(state, action: PayloadAction<Patient | null>) {
      localStorage.setItem("currentPatient", JSON.stringify(action.payload));
      state.currentPatient = action.payload;
    },
    addRecipeMedicament(state, action: PayloadAction<RecipeMedicament>) {
      const index = state.newRecipeMedicaments.findIndex(
        (listItem) => listItem.medicament.id === action.payload.medicament.id
      );
      if (index !== -1) {
        const newList = replaceItemAtIndex(
          state.newRecipeMedicaments,
          index,
          action.payload
        );
        state.newRecipeMedicaments = newList;
      } else {
        state.newRecipeMedicaments.push(action.payload);
      }
    },
    deleteRecipeMedicament(state, action: PayloadAction<string>) {
      const index = state.newRecipeMedicaments.findIndex(
        (listItem) => listItem.medicament.id === action.payload
      );
      const newList = removeItemAtIndex(state.newRecipeMedicaments, index);
      state.newRecipeMedicaments = newList;
    },
    recipeMedicamentAction(
      state,
      action: PayloadAction<RecipeMedicament | null>
    ) {
      state.currentRecipeMedicament = action.payload;
    },
    addAllRecipeMedicament(state, action: PayloadAction<RecipeMedicament[]>) {
      localStorage.setItem(
        "newRecipeMedicaments",
        JSON.stringify(action.payload)
      );
      state.newRecipeMedicaments = action.payload;
    },
    clearRecipeMedicament(state) {
      state.newRecipeMedicaments = [];
    },
    addRecipe(state, action: PayloadAction<RecipeDto | null>) {
      localStorage.setItem("currentRecipe", JSON.stringify(action.payload));
      state.currentRecipe = action.payload;
    },
    addMedicament(state, action: PayloadAction<Medicament | null>) {
      state.medicament = action.payload;
    },
  },
});

export const patientActions = patientSlice.actions;
export const patientReducer = patientSlice.reducer;
