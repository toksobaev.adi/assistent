import { createApi } from "@reduxjs/toolkit/query/react";
import { baseQuery } from "../../utils/baseQuery";
import { injectToken } from "../../utils/injectToken";
import { MedicamentGroupResult } from "models/index";

export const medicamentApi = createApi({
  reducerPath: "medicamentApi",
  baseQuery,
  refetchOnFocus: true,
  endpoints: (build) => ({
    medicaments: build.query<MedicamentGroupResult, string>({
      query: () => ({
        url: "/Medicament/GetGroups",
        headers: injectToken(),
      }),
    }),
  }),
});

export const { useMedicamentsQuery } = medicamentApi;
