import { createSlice } from "@reduxjs/toolkit";

interface MedicamentState {}

const initialState: MedicamentState = {};

export const medicamentSlice = createSlice({
  name: "medicament",
  initialState,
  reducers: {},
});

export const medicamentActions = medicamentSlice.actions;
export const medicamentReducer = medicamentSlice.reducer;
