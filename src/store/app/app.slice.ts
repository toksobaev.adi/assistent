import { PayloadAction, createSlice } from "@reduxjs/toolkit";

interface AppState {
  sessionId: string | null;
}

const initialState: AppState = {
  sessionId: localStorage.getItem("sessionId")
};

export const appSlice = createSlice({
  name: "app",
  initialState,
  reducers: {
    setSessionId(state, action: PayloadAction<string>) {
      localStorage.setItem("sessionId", action.payload);
      state.sessionId = action.payload;
    },
    removeSessionId(state) {
      localStorage.removeItem("sessionId");
      state.sessionId = null;
    },
  },
});

export const appActions = appSlice.actions;
export const appReducer = appSlice.reducer;
