import { createApi } from "@reduxjs/toolkit/query/react";
import { baseQuery } from "../../utils/baseQuery";
import { LoginDto, LoginResult } from "models/index";

export const appApi = createApi({
  reducerPath: "appApi",
  baseQuery,
  refetchOnFocus: true,
  endpoints: (build) => ({
    login: build.mutation<LoginResult, LoginDto>({
      query: (body) => ({
        url: `/Auth/Login`,
        method: "POST",
        body,
      }),
    }),
  }),
});

export const { useLoginMutation } = appApi;
