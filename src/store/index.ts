import { configureStore } from "@reduxjs/toolkit";
import { appApi } from "./app/app.api";
import { medicamentApi } from "./medicament/medicament.api";
import { patientApi } from "./patient/patient.api";
import { setupListeners } from "@reduxjs/toolkit/query";
import { appReducer } from "./app/app.slice";
import { medicamentReducer } from "./medicament/medicament.slice";
import { patientReducer } from "./patient/patient.slice";

export const store = configureStore({
  reducer: {
    [appApi.reducerPath]: appApi.reducer,
    [medicamentApi.reducerPath]: medicamentApi.reducer,
    [patientApi.reducerPath]: patientApi.reducer,
    app: appReducer,
    medicament: medicamentReducer,
    patient: patientReducer,
  },
  middleware: (getDefaultMiddleware) =>
    getDefaultMiddleware()
      .concat(appApi.middleware, medicamentApi.middleware)
      .concat(patientApi.middleware),
});

setupListeners(store.dispatch);

export type RootState = ReturnType<typeof store.getState>;
