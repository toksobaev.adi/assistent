import { useDispatch } from "react-redux";
import { bindActionCreators } from "@reduxjs/toolkit";
import { appActions } from "store/app/app.slice";
import { medicamentActions } from "store/medicament/medicament.slice";
import { patientActions } from "store/patient/patient.slice";

const actions = {
  ...appActions,
  ...medicamentActions,
  ...patientActions,
};

export const useActions = () => {
  const dispatch = useDispatch();
  return bindActionCreators(actions, dispatch);
};
