import { Swiper, SwiperSlide } from "swiper/react";

// Import Swiper styles
import "swiper/css";
import "swiper/css/pagination";
import "swiper/css/navigation";

// import required modules
// @ts-ignore
import { Pagination, Navigation } from "swiper";
import { Button, Grid, Stack } from "@mui/material";
import { LazyImage } from "components/LazyImage";
import logo from "../../assets/img/logo.png";
import service from "../../assets/img/service.jpg";
import service1 from "../../assets/img/service-1.jpg";
import service2 from "../../assets/img/service-2.jpg";
import { useState } from "react";
import PhoneCallbackRoundedIcon from "@mui/icons-material/PhoneCallbackRounded";
import BoltRoundedIcon from "@mui/icons-material/BoltRounded";
import NotificationsRoundedIcon from "@mui/icons-material/NotificationsRounded";
import OutboundRoundedIcon from "@mui/icons-material/OutboundRounded";
import "./Home.scss";

function Home() {
  const [swiperRef, setSwiperRef] = useState<any | null>(null);

  return (
    <Swiper
      onSwiper={(swiper) => setSwiperRef(swiper)}
      navigation={true}
      pagination={{ clickable: true }}
      modules={[Pagination, Navigation]}
      className="mainSwiper"
    >
      <SwiperSlide className="mainSwiper__item">
        <div className="mainSwiper__count">
          1 из {swiperRef?.slides?.length}
        </div>
        <Grid container spacing={2}>
          <Grid item xs={8}>
            <div className="services">
              <LazyImage src={logo} className="logo" />
              <div className="title">Опасаетесь пропустить важный звонок?</div>
              <p className="desc services__desc">
                Нет времени отвечать на все входящие звонки? Всегда хотите быть
                в курсе, кто и зачем вам звонил?
              </p>
              <div className="services__row">
                <div className="services__item active">
                  <PhoneCallbackRoundedIcon
                    sx={{ fontSize: "33px", color: "#82B955" }}
                  />
                  <div className="services__label">Важный звонок?</div>
                </div>
                <div className="services__item">
                  <BoltRoundedIcon
                    sx={{ fontSize: "33px", color: "#82B955" }}
                  />
                  <div className="services__label">Крутое решение</div>
                </div>
                <div className="services__item">
                  <NotificationsRoundedIcon
                    sx={{ fontSize: "33px", color: "#82B955" }}
                  />
                  <div className="services__label">Уведомление</div>
                </div>
                <div className="services__item">
                  <OutboundRoundedIcon
                    sx={{ fontSize: "33px", color: "#82B955" }}
                  />
                  <div className="services__label">Бесплатное подключение</div>
                </div>
              </div>
              <Stack spacing={2} direction="row" className="services__btn-row">
                <Button
                  variant="outlined"
                  sx={{
                    color: "#000000",
                    borderColor: "#000000",
                    "&:hover": { borderColor: "#000000" },
                  }}
                >
                  Войти
                </Button>
                <Button variant="contained" sx={{ color: "#ffffff" }}>
                  Подключить услугу
                </Button>
              </Stack>
            </div>
          </Grid>
          <Grid item xs={4}>
            <div className="services__img">
              <LazyImage src={service} />
            </div>
          </Grid>
        </Grid>
      </SwiperSlide>
      <SwiperSlide className="mainSwiper__item">
        <div className="mainSwiper__count">
          2 из {swiperRef?.slides?.length}
        </div>
        <Grid container spacing={2}>
          <Grid item xs={8}>
            <div className="services">
              <LazyImage src={logo} className="logo" />
              <div className="title">Решение есть!</div>
              <p className="desc services__desc">
                С услугой “Ассистент” от MEGA Вы сможете получать уведомления о
                пропущенных звонках, а также слушать голосовые сообщения,
                звонивших Вам абонентов, когда Вам удобно!
              </p>
              <div className="services__row">
                <div className="services__item">
                  <PhoneCallbackRoundedIcon
                    sx={{ fontSize: "33px", color: "#82B955" }}
                  />
                  <div className="services__label">Важный звонок?</div>
                </div>
                <div className="services__item active">
                  <BoltRoundedIcon
                    sx={{ fontSize: "33px", color: "#82B955" }}
                  />
                  <div className="services__label">Крутое решение</div>
                </div>
                <div className="services__item">
                  <NotificationsRoundedIcon
                    sx={{ fontSize: "33px", color: "#82B955" }}
                  />
                  <div className="services__label">Уведомление</div>
                </div>
                <div className="services__item">
                  <OutboundRoundedIcon
                    sx={{ fontSize: "33px", color: "#82B955" }}
                  />
                  <div className="services__label">Бесплатное подключение</div>
                </div>
              </div>
              <Stack spacing={2} direction="row" className="services__btn-row">
                <Button
                  variant="outlined"
                  sx={{
                    color: "#000000",
                    borderColor: "#000000",
                    "&:hover": { borderColor: "#000000" },
                  }}
                >
                  Войти
                </Button>
                <Button variant="contained" sx={{ color: "#ffffff" }}>
                  Подключить услугу
                </Button>
              </Stack>
            </div>
          </Grid>
          <Grid item xs={4}>
            <div className="services__img">
              <LazyImage src={service1} />
            </div>
          </Grid>
        </Grid>
      </SwiperSlide>
      <SwiperSlide className="mainSwiper__item">
        <div className="mainSwiper__count">
          2 из {swiperRef?.slides?.length}
        </div>
        <Grid container spacing={2}>
          <Grid item xs={8}>
            <div className="services">
              <LazyImage src={logo} className="logo" />
              <div className="title">Удобные уведомления</div>
              <p className="desc services__desc">
                С услугой “Ассистент” от MEGA Вы сможете получать уведомления о
                пропущенных звонках, а также слушать голосовые сообщения,
                звонивших Вам абонентов, когда Вам удобно!
              </p>
              <div className="services__row">
                <div className="services__item">
                  <PhoneCallbackRoundedIcon
                    sx={{ fontSize: "33px", color: "#82B955" }}
                  />
                  <div className="services__label">Важный звонок?</div>
                </div>
                <div className="services__item">
                  <BoltRoundedIcon
                    sx={{ fontSize: "33px", color: "#82B955" }}
                  />
                  <div className="services__label">Крутое решение</div>
                </div>
                <div className="services__item active">
                  <NotificationsRoundedIcon
                    sx={{ fontSize: "33px", color: "#82B955" }}
                  />
                  <div className="services__label">Уведомление</div>
                </div>
                <div className="services__item">
                  <OutboundRoundedIcon
                    sx={{ fontSize: "33px", color: "#82B955" }}
                  />
                  <div className="services__label">Бесплатное подключение</div>
                </div>
              </div>
              <Stack spacing={2} direction="row" className="services__btn-row">
                <Button
                  variant="outlined"
                  sx={{
                    color: "#000000",
                    borderColor: "#000000",
                    "&:hover": { borderColor: "#000000" },
                  }}
                >
                  Войти
                </Button>
                <Button variant="contained" sx={{ color: "#ffffff" }}>
                  Подключить услугу
                </Button>
              </Stack>
            </div>
          </Grid>
          <Grid item xs={4}>
            <div className="services__img">
              <LazyImage src={service2} />
            </div>
          </Grid>
        </Grid>
      </SwiperSlide>
      <SwiperSlide className="mainSwiper__item">
        <div className="mainSwiper__count">
          2 из {swiperRef?.slides?.length}
        </div>
        <Grid container spacing={2}>
          <Grid item xs={8}>
            <div className="services">
              <LazyImage src={logo} className="logo" />
              <div className="title">Вы можете уведомлять</div>
              <p className="desc services__desc">
                Вы можете уведомлять других абонентов о своем появлении в сети
                (действует для абонентов MEGA). Подключение услуги бесплатно.
                Абонентская плата: 1.99 сом (с НДС, без НсП)
              </p>
              <div className="services__row">
                <div className="services__item">
                  <PhoneCallbackRoundedIcon
                    sx={{ fontSize: "33px", color: "#82B955" }}
                  />
                  <div className="services__label">Важный звонок?</div>
                </div>
                <div className="services__item">
                  <BoltRoundedIcon
                    sx={{ fontSize: "33px", color: "#82B955" }}
                  />
                  <div className="services__label">Крутое решение</div>
                </div>
                <div className="services__item">
                  <NotificationsRoundedIcon
                    sx={{ fontSize: "33px", color: "#82B955" }}
                  />
                  <div className="services__label">Уведомление</div>
                </div>
                <div className="services__item active">
                  <OutboundRoundedIcon
                    sx={{ fontSize: "33px", color: "#82B955" }}
                  />
                  <div className="services__label">Бесплатное подключение</div>
                </div>
              </div>
              <Stack spacing={2} direction="row" className="services__btn-row">
                <Button
                  variant="outlined"
                  sx={{
                    color: "#000000",
                    borderColor: "#000000",
                    "&:hover": { borderColor: "#000000" },
                  }}
                >
                  Войти
                </Button>
                <Button variant="contained" sx={{ color: "#ffffff" }}>
                  Подключить услугу
                </Button>
              </Stack>
            </div>
          </Grid>
          <Grid item xs={4}>
            <div className="services__img">
              <LazyImage src={service2} />
            </div>
          </Grid>
        </Grid>
      </SwiperSlide>
    </Swiper>
  );
}

export default Home;
