import React, { useState, useEffect } from "react";
import "./LazyImage.scss";

const placeHolder =
  "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAASAAAAEgCAMAAAAjXV6yAAAASFBMVEUAAADx9Pnv7//w8Pjv7+/x8/jx8vjx9Pnw8/jv8vbx9Pjv8fbv8/jv8/fz9Pnv8vfx8/nx8/jx8/jv8vby8/jz8/nw8/jx8/gSA4MoAAAAF3RSTlMAzxAgEO/fn2Aw33CQQI+A769wUL9/v+dIenkAAAa6SURBVHja7NxRd6MgEIbhUYIKqInRnPn//3QvejbbRVpMxArT77ltb/KeCcYIIQAAAAAAAAAAAAAAAAAAAMiP6saparggTTVdZ0U/YhiLSvOZbQc6WP3QXDTbKXpCnhA9HpboLiDPxxTREdTCYlhFhPH5zs1RYiML86CkWhanpYQqFqjC/PzUDD1YqJGSuLNYjhJQoq7v/7sp2s+yYAvt1rFojnZSlkN025mailGbrtUccquP+ATduILiPHUNB1zTr9D6ToVyOvkIdbzSKCqWalKPkGVfVXAfIlWx70Y7DKLm54sZGlLehOnC+4RW1T7lO6zY9fkfxx67ozZ7GhKgYY+id83s6UgAl+5VXVcLvgQXnWwRmlbfMIngX3omelcl8R22/vRbJVvODIlgkl3G2FPiLWrAhT3JApEQCIRACPQZAkUgUAQCRSBQBAJFIFAEAkUgUAQCRSBQBAJFIFAEAkWIDmRc+3GyrFlGl8tXttkEGkbtH9jKolEmgYaFA6oMHo1kEeiZZ8WevgEig0D1yN8Ya3qJvEB/t35mOkSnBzKaI26GXiAtUMcBOW0UOTmQ4bB8nmOfG0jp5OckRAWqLW9ka9pIVKDxgONakgIpfsFA20gKZI84jCQoUMdcwAidGKhiLmCEzgtkeE33syIyc5vPCJ0XKBBhqp/rd+CvPW0hJ5Bl393bd53FzuvTApnYYSyXxw3HaYFc9LcNpvcO1UoJ1EePiVx0Dpv3Twu0xF99/84udzGBGu//5/gBIktbSAm0YQVWOTyMzCZQrk9rEaiUQIpWzK8OhEX6xXv5Pn639oecO1xtEAiCAHxq1EpNIQ2N7/+mDf3RhQRvlFt0Zm/+iSD4cXosrvtIWxIFaIRzMTqKapWn1Li9EdZdakxoftFP5cVq6vMDL78XjkkF5wHNy1vG/72+vT0PGWrVE4Gui8UQ7lNK3f2rp/n+fAwQrudxGofp8c3YCQFdgYhni0f7ZQ1ZMkD7ltBQ5NPYhZpWBmjXEuqKfUxIBcg2MpzZwceEVIAuw+YHrPXwMSERoM0NVH3n42NCIkDOLXjYx4REgJybOLGPCYkAbWgD7l3XjwmJAKVuAO/nztfHhESA0mXO7u+tt48JiQDlFtHjmvx9TEgFKKWPxpPHfJCQDFBK0zi8dpshHuyDhXSAnpk+58ef0tCM9ktmkQ8WUgLyi/lgoTqBzAcL1QhkPluE6gNa8WmaFaHagNo1iMvaibqA2nWGVaGagDI+GaF6gDI+WaFagDI+QKgOoIwPFKoBCPgAofhAwAcKRQdCPlgoNhD2wUKRgYDPRqG4QMBns1BUIOCzQygmEPDZJRQRCPjsFIoHBHx2C0UDQj77hWIBYZ/9QpGAMj4FQnGAMj5FQlGAgE+BUAwg4FMkFAEI+BQK6QMBn2IhdSDg4yCkDQR8XISUgYCPk5AuEPBxE1IFAj5+QqJAwMdRSBPI1ceEwgA5+5hQECB3HxMKAQR8yoQCAAGfUiF5IOBTLiQOBHw8hKSBgI+PkDAQ8PESkgUCPn5CokDAx1NIEgj4+AoJAgEfbyE5IODjLyQGBH38haSADvQxISGgQ31MSAboYB8TEgE63MeEJIBO8DEhAaAzfEyI/6vGOT4mxA7k5XP+fS0vCeJDDnS+DzcQgQ81EIMPMxCFDzEQhw8vEIkPLRCLDysQjQ8pEI8PJxCRDyUQkw8jEJUPIRCXDx8QmQ8dEJsPGxCdDxkQnw8XEKEPFRCjDxMQpQ8REKcPDxCpDw0Qqw8LEK0PCRCvz297d5PcIAyDYVig+ifGGQLpVPe/abMW6QasxtZ8zwE84h15YEcfgTru00Wgnvv0EKjrPh0E6rvP5wN13ufjgXrvYxfISR+7QE762AVy0scukJM+doGc9LEL5KSPXSAnfewCOeljF8hJH7tATvrYBXLSxy6Qkz52gWYffb6aBYrqoJuLPof/vRc6Sz92dtGHsp6TztrUSclFH0pq0I3Oquqkx+yhDwU16dpsF2Xx0CeL8qSzWJTioA8VUZhOi6Is4/fZRYl0XhXlwaP34SBKovPuohUevE8R7U4XBNEmHrrPJFqkK1Y5KDxwnyIHma74CnK0jNpnD3LwYGqwQkrJA/aZlyJvVKIWK6SFlEfqM99yCvJOZLpoEdcyXfYtjsU2X1ZuBaYXXLI/7dREFadWaiSJS4mamcShiV6wQ1b7o63iTKXGFldv+7BTcxzFjR8mC9lJorCTEa4O7llYZ7LDo2+RymPhnoZtFOud/gU/120aKlOctpqZAAAAAAAAAAAAAAAAAAAAoDu/d0OLn3qe72oAAAAASUVORK5CYII=";

type Props = {
  src: string;
  alt?: string;
  className?: string;
  placeholder?: string;
  width?: string | number;
  height?: string | number;
};
export const LazyImage: React.FC<Props> = ({
  src,
  alt = "",
  className = "",
  placeholder = placeHolder,
  width,
  height,
}) => {
  const [imageSrc, setImageSrc] = useState(placeholder);
  const [imageRef, setImageRef] = useState<HTMLImageElement | null>(null);

  const onLoad = (event: React.SyntheticEvent<HTMLImageElement, Event>) => {
    event.currentTarget.classList.add("loaded");
  };

  const onError = (event: React.SyntheticEvent<HTMLImageElement, Event>) => {
    event.currentTarget.classList.add("has-error");
  };

  useEffect(() => {
    let observer: IntersectionObserver;
    let didCancel = false;

    if (imageRef && imageSrc !== src) {
      if (IntersectionObserver) {
        observer = new IntersectionObserver(
          (entries) => {
            entries.forEach((entry) => {
              if (
                !didCancel &&
                (entry.intersectionRatio > 0 || entry.isIntersecting)
              ) {
                setImageSrc(src);
                observer.unobserve(imageRef);
              }
            });
          },
          {
            threshold: 0.01,
            rootMargin: "75%",
          }
        );
        observer.observe(imageRef);
      } else {
        // Old browsers fallback
        setImageSrc(src);
      }
    }
    return () => {
      didCancel = true;
      // on component cleanup, we remove the listner
      if (observer && observer.unobserve) {
        observer.unobserve(imageRef!);
      }
    };
  }, [src, imageSrc, imageRef]);

  return (
    <img
      ref={setImageRef}
      src={imageSrc}
      alt={alt}
      className={`lazy-image ${className}`}
      onLoad={onLoad}
      onError={onError}
      width={width}
      height={height}
    />
  );
};
