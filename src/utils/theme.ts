import { createTheme } from "@mui/material/styles";

const mainTheme = createTheme({
  palette: {
    primary: {
      main: "#82B955",
    },
  },
});

export default mainTheme;
