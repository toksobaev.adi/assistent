import { fetchBaseQuery } from "@reduxjs/toolkit/dist/query";

export const initialHeaders = {
  "Access-Control-Allow-Origin": "*",
  Accept: "application/json",
  "Content-Type": "application/json; charset=utf-8",
  "ngrok-skip-browser-warning": "ngrok",
};

export const baseQuery = fetchBaseQuery({
  baseUrl: `${import.meta.env.VITE_WEBAPI_URL}/api`,
  headers: initialHeaders,
});
