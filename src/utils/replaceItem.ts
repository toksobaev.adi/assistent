export function replaceItemAtIndex<Type>(arr: Type[], index: number, newValue: Type): Type[] {
  return [...arr.slice(0, index), newValue, ...arr.slice(index + 1)];
}

export function removeItemAtIndex<Type>(arr: Type[], index: number): Type[] {
  return [...arr.slice(0, index), ...arr.slice(index + 1)];
}