export type MedicamentGroup = {
  groupName?: string;
  medicaments: Medicament[];
};

export interface Medicament {
  id: string;
  commercialName?: string;
  commercialNameWithDoses?: string;
  activeSubstance?: string;
}

export type ResultError = {
  errorMessage?: string;
  errorCode?: string;
  additionalInfo?: string;
};

export type MedicamentGroupResult = {
  ok: boolean;
  result: MedicamentGroup[];
};

export type PatientDtoArrayBaseResult = {
  ok: boolean;
  result: Patient[];
};

export interface Patient {
  id?: string;
  surname?: string;
  name?: string;
  patronymic?: string;
  diagnosis?: string;
  phone?: string;
  birthDate?: any;
  age: number;
}

export type PatientDto = Omit<Patient, 'gender'>

export type GuidBaseResult = {
  ok: boolean;
  result: string;
};

export type CounterResult = {
  ok: boolean;
  result: {
    patientsCount:	number;
    medicamentsCount:	number;
  };
};

export type RecipeDto = {
  id?: string;
  title?: string;
  description?: string;
  patientId: string;
  recipeMedicaments: RecipeMedicament[]
}

export type RecipeMedicament = {
  medicament: Medicament;
  days: number;
  receptionOption: number;
}

export type RecipeDtoArrayBaseResult = {
  ok: boolean;
  result: RecipeDto[];
}

export type LoginDto = {
  phone: string;
  password: string;
}

export type LoginResult = {
  sessionId: string;
  fullName?: string;
}
  
