import React, { Suspense } from "react";
import { Routes, Route } from "react-router-dom";
import { Loader } from "components/Loader";

const HomePage = React.lazy(() => import("pages/Home/Home"));

function MainRouter() {
  return (
    <Suspense fallback={<Loader />}>
      <Routes>
        <Route path="/" element={<HomePage />} />
      </Routes>
    </Suspense>
  );
}

export default MainRouter;
