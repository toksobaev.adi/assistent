import i18n from "i18next";
import { initReactI18next } from "react-i18next";

const lng =
  localStorage.getItem("i18nextLng") || import.meta.env.VITE_DEFAULT_LANG;

//Import all translation files
import en from "./locales/en/translation.json";
import ru from "./locales/ru/translation.json";
import kg from "./locales/kg/translation.json";

//---Using translation
const resources = {
  en: {
    translation: en,
  },
  ru: {
    translation: ru,
  },
  kg: {
    translation: kg,
  },
};

i18n
  .use(initReactI18next) // passes i18n down to react-i18next
  .init({
    resources,
    lng, //default language
    fallbackLng: lng,
    debug: true,
    interpolation: {
      escapeValue: false,
    },
  });
export default i18n;
